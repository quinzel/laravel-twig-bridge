## 0.10

Forked from [ascii-soup/TwigBridge](https://github.com/ascii-soup/TwigBridge) into Quinzel/TwigBridge

- Full support for Twig 2.x
- Laravel >= 5.5 support with autoregistering 

## 0.8

Changes since 0.7

 - (Breaking) Normalize view events for included templates (folder/file.twig -> folder.file)
 - (initial) Twig 2.x support
 
